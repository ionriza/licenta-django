from django.db import models
from django.conf import settings

from django.contrib.auth.models import User


class ExtendUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    # Folosit la comanda pentru cei care nu au cont
    nume = models.CharField(max_length=200)
    adresa_livrare = models.CharField(max_length=200)
    judet = models.CharField(max_length=200)
    oras = models.CharField(max_length=200)
    cod_postal = models.CharField(max_length=200)
    telefon = models.CharField(max_length=200)

    def __str__(self):
        return self.nume

class ComandaProdus(models.Model):
    nume = models.CharField(max_length=200)
    cantitate = models.IntegerField()
    pret_unitar = models.IntegerField()
    pret_total = models.IntegerField()
    poza = models.ImageField()

    def __str__(self):
        return f"{self.nume} | {self.cantitate} | {self.pret_unitar} | {self.pret_total}"

class Comanda(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE,
                             null=True)
    extend_user = models.ForeignKey(ExtendUser,
                                    on_delete=models.CASCADE,
                                    null=True)
    client = models.BooleanField()
    pret_total = models.IntegerField()
    produse = models.ManyToManyField(ComandaProdus, blank=True)

    statusuri = (
        ('Initiata', 'Initiata'),
        ('Procesare', 'Procesare'),
        ('Livrare', 'Livrare'),
        ('Livrata', 'Livrata'),
        ('Anulata', 'Anulata'),
    )
    status = models.CharField(max_length=10, choices=statusuri, default='Initiata')

    def proceseaza_produse(self):
        return ",".join([p.nume for p in self.produse.all()])

    class Meta:
        verbose_name_plural = "Comenzi"

class Review(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    score = models.IntegerField()
    comment = models.CharField(max_length=200)


class Procesor(models.Model):
    nume = models.CharField(max_length=200)
    pret = models.IntegerField()
    cantitate = models.IntegerField()
    separat = models.BooleanField()
    socket = models.CharField(max_length=200)
    nucleu = models.CharField(max_length=200)
    nr_nuclee = models.IntegerField()
    nr_threaduri = models.IntegerField()
    frecventa = models.CharField(max_length=200)
    poza = models.ImageField(upload_to='procesor', default='dummy_image.jpg')
    reviews = models.ManyToManyField(Review, blank=True)

    class Meta:
        verbose_name_plural = "Procesoare"

    def __str__(self):
        return f"{self.nume}"


class PlacaDeBaza(models.Model):
    nume = models.CharField(max_length=200)
    pret = models.IntegerField()
    cantitate = models.IntegerField()
    separat = models.BooleanField()
    format_placa_de_baza = models.CharField(max_length=200)
    soclu_procesor = models.CharField(max_length=200)
    producator_chipset = models.CharField(max_length=200)
    tip_memorie_suportata = models.CharField(max_length=200)
    memorie_maxima_suportata = models.CharField(max_length=200)
    memorie_sloturi = models.CharField(max_length=200)
    poza = models.ImageField(upload_to='placa_de_baza',
                             default='dummy_image.jpg')
    reviews = models.ManyToManyField(Review, blank=True)

    class Meta:
        verbose_name_plural = "Placi de baza"

    def __str__(self):
        return f"{self.nume}"


class PlacaVideo(models.Model):
    nume = models.CharField(max_length=200)
    pret = models.IntegerField()
    cantitate = models.IntegerField()
    separat = models.BooleanField()
    interfata = models.CharField(max_length=200)
    rezolutie_maxima = models.CharField(max_length=200)
    producator_chipset = models.CharField(max_length=200)
    tip_memorie = models.CharField(max_length=200)
    dimensiune_memorie = models.CharField(max_length=200)
    bus_memorie = models.CharField(max_length=200)
    poza = models.ImageField(upload_to='placa_video',
                             default='dummy_image.jpg')
    reviews = models.ManyToManyField(Review, blank=True)

    class Meta:
        verbose_name_plural = "Placi video"

    def __str__(self):
        return f"{self.nume}"


class Memorie(models.Model):
    nume = models.CharField(max_length=200)
    pret = models.IntegerField()
    cantitate = models.IntegerField()
    separat = models.BooleanField()
    tip_memorie = models.CharField(max_length=200)
    capacitate = models.CharField(max_length=200)
    frecventa = models.CharField(max_length=200)
    poza = models.ImageField(upload_to='memorie', default='dummy_image.jpg')
    reviews = models.ManyToManyField(Review, blank=True)

    class Meta:
        verbose_name_plural = "Memorii"

    def __str__(self):
        return f"{self.nume}"


class Stocare(models.Model):
    nume = models.CharField(max_length=200)
    pret = models.IntegerField()
    cantitate = models.IntegerField()
    separat = models.BooleanField()
    tip = models.CharField(max_length=200)
    capacitate = models.CharField(max_length=200)
    viteza_scriere = models.CharField(max_length=200)
    viteza_citire = models.CharField(max_length=200)
    poza = models.ImageField(upload_to='stocare', default='dummy_image.jpg')
    reviews = models.ManyToManyField(Review, blank=True)

    class Meta:
        verbose_name_plural = "Stocare"

    def __str__(self):
        return f"{self.nume}"


class Sursa(models.Model):
    nume = models.CharField(max_length=200)
    pret = models.IntegerField()
    cantitate = models.IntegerField()
    separat = models.BooleanField()
    tip = models.CharField(max_length=200)
    putere = models.CharField(max_length=200)
    numar_ventilatoare = models.CharField(max_length=200)
    voltaj_amperaj = models.CharField(max_length=200)
    eficienta = models.CharField(max_length=200)
    certificare = models.CharField(max_length=200)
    poza = models.ImageField(upload_to='sursa', default='dummy_image.jpg')
    reviews = models.ManyToManyField(Review, blank=True)

    class Meta:
        verbose_name_plural = "Surse"

    def __str__(self):
        return f"{self.nume}"


class Carcasa(models.Model):
    nume = models.CharField(max_length=200)
    pret = models.IntegerField()
    cantitate = models.IntegerField()
    separat = models.BooleanField()
    tip_carcasa = models.CharField(max_length=200)
    culoare = models.CharField(max_length=200)
    dimensiuni = models.CharField(max_length=200)
    poza = models.ImageField(upload_to='carcasa', default='dummy_image.jpg')
    reviews = models.ManyToManyField(Review, blank=True)

    class Meta:
        verbose_name_plural = "Carcase"

    def __str__(self):
        return f"{self.nume}"


class Software(models.Model):
    nume = models.CharField(max_length=200)
    pret = models.IntegerField()
    cantitate = models.IntegerField()
    separat = models.BooleanField()
    licentiere = models.CharField(max_length=200)
    licenta_pentru = models.CharField(max_length=200)
    versiune = models.CharField(max_length=200)
    platforma = models.CharField(max_length=200)
    poza = models.ImageField(upload_to='software', default='dummy_image.jpg')
    reviews = models.ManyToManyField(Review, blank=True)

    class Meta:
        verbose_name_plural = "Software"

    def __str__(self):
        return f"{self.nume}"


class Calculator(models.Model):
    nume = models.CharField(max_length=200)
    pret = models.IntegerField()
    cantitate = models.IntegerField()
    procesor = models.ForeignKey(Procesor, on_delete=models.CASCADE)
    placa_de_baza = models.ForeignKey(PlacaDeBaza, on_delete=models.CASCADE)
    placa_video = models.ForeignKey(PlacaVideo, on_delete=models.CASCADE)
    memorie = models.ForeignKey(Memorie, on_delete=models.CASCADE)
    stocare = models.ForeignKey(Stocare, on_delete=models.CASCADE)
    sursa = models.ForeignKey(Sursa, on_delete=models.CASCADE)
    carcasa = models.ForeignKey(Carcasa, on_delete=models.CASCADE)
    software = models.ForeignKey(Software, on_delete=models.CASCADE)
    poza = models.ImageField(upload_to='calculator', default='dummy_image.jpg')
    reviews = models.ManyToManyField(Review, blank=True)

    class Meta:
        verbose_name_plural = "Calculatoare"

    def __str__(self):
        return f"{self.nume}"