from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from django import forms

from .models import ExtendUser

class CreateUserForm(UserCreationForm):

	class Meta:
		model = User
		fields = ['username', 'email', 'first_name', 'last_name', 'password1', 'password2']
	
class DetaliiLivrareUser(forms.ModelForm):
	
	class Meta:
		model = ExtendUser
		fields = ['adresa_livrare', 'judet', 'oras', 'cod_postal', 'telefon']
		widgets = {
			'adresa_livrare': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Bulevardul Iuliu Maniu, nr 1-3, Bucuresti', 'required': True}),
			'judet': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Bucuresti', 'required': True}),
			'oras': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Bucuresti', 'required': True}),
			'cod_postal': forms.TextInput(attrs={'class': 'form-control', 'placeholder': '061071', 'required': True}),
			'telefon': forms.TextInput(attrs={'class': 'form-control', 'placeholder': '0712345678', 'required': True})
		}

class DetaliiLivrareNonUser(forms.ModelForm):
	
	class Meta:
		model = ExtendUser
		fields = ['nume', 'adresa_livrare', 'oras', 'judet', 'cod_postal', 'telefon']
		widgets = {
			'nume': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ion Popescu', 'required': True}),
			'adresa_livrare': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Bulevardul Iuliu Maniu, nr 1-3, Bucuresti', 'required': True}),
			'oras': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Bucuresti', 'required': True}),
			'judet': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Bucuresti', 'required': True}),
			'cod_postal': forms.TextInput(attrs={'class': 'form-control', 'placeholder': '061071', 'required': True}),
			'telefon': forms.TextInput(attrs={'class': 'form-control', 'placeholder': '0712345678', 'required': True})
		}
