from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('produse/<str:categorie>/', views.pagina_categorie, name='categorie'),
    path('<str:categorie>/<int:id>/', views.produs, name='produs'),
    path('<str:categorie>/<int:id>/review/', views.review, name='review'),
    path('<str:categorie>/<int:id>/compara/', views.compara, name='compara'),
    path('cos/', views.cos, name='cos'),
    path('cos/<str:metoda>/<str:categorie>/<int:id>/', views.modifica_cos, name='modifica_cos'),
    path('comenzi/', views.comenzile_mele, name='comenzi'),
    path('checkout/', views.checkout, name='checkout'),
    path('register/', views.pagina_inregistrare, name='register'),
    path('login/', views.pagina_logare, name="login"), 
    path('logout/', views.pagina_delogare, name="logout"),
    path('user/', views.pagina_user, name="user") 
]