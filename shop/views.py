from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.http import Http404
from django.core.paginator import Paginator

from .models import Calculator, Carcasa, Memorie, PlacaDeBaza, PlacaVideo, Procesor, Software, Stocare, Sursa, ExtendUser, Review, ComandaProdus, Comanda
from .forms import CreateUserForm, DetaliiLivrareUser, DetaliiLivrareNonUser

from django.contrib import messages

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from django.forms.models import model_to_dict

produse = [
    Procesor, PlacaDeBaza, PlacaVideo, Memorie, Stocare, Sursa, Carcasa,
    Software, Calculator
]
plural_name_models = {
    p._meta.verbose_name_plural.lower().replace(' ', '-'): p
    for p in produse
}

def interogare_produs(categorie, id):
    try:
        p = plural_name_models[categorie].objects.get(id=id)
    except KeyError:
        raise Http404("Nu exista aceasta categorie")
    except plural_name_models[categorie].DoesNotExist:
        raise Http404("Produsul nu exista")
    return p

def index(request):
    context = {"data": [], 'user': request.user}
    for p in produse:
        context['data'].append({
            'name':
            str(p.__name__),
            'pluralname':
            p._meta.verbose_name_plural,
            'queries':
            p.objects.all()[:3],
            'urlname':
            p._meta.verbose_name_plural.lower().replace(' ', '-')
        })
    return render(request, 'index.html', context)

def pagina_categorie(request, categorie):
    context = {"data": [], 'user': request.user, 'categorie_nume': categorie}
    produse = plural_name_models[categorie].objects.all()
    paginator = Paginator(produse, 9)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context['page_obj'] = page_obj
    context['data'] = { 'name': str(plural_name_models[categorie].__name__),
        'pluralname': plural_name_models[categorie]._meta.verbose_name_plural,
        'urlname': plural_name_models[categorie]._meta.verbose_name_plural.lower().replace(' ', '-'),
        'queries': produse
    }
    return render(request, 'categorie.html', context)

def pagina_inregistrare(request):
    if request.user.is_authenticated:
        return redirect('index')
    else:
        form = CreateUserForm()
        detaliiForm = DetaliiLivrareUser()
        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            detaliiForm = DetaliiLivrareUser(request.POST)
            if form.is_valid() and detaliiForm.is_valid():
                userObj = form.save()
                user = authenticate(request, username=form.cleaned_data['username'],
                                    password=form.cleaned_data['password1'])
                detalii = detaliiForm.save(commit=False)
                detalii.user = userObj
                detalii.nume = userObj.first_name + ' ' + userObj.last_name
                detalii.save()
                login(request, user)
                return redirect('index')

    return render(request, 'register.html', {
        'formUser': form,
        'formDetalii': detaliiForm
    })

def comenzile_mele(request):
    comenzi = Comanda.objects.filter(user=request.user)
    for c in comenzi:
        print(c.produse)
    return render(request, 'comenzilemele.html', {'comenzi': comenzi})

def pagina_logare(request):
    if request.user.is_authenticated:
        return redirect('index')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password =request.POST.get('password')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('index')
            else:
                messages.info(request, 'Username OR password is incorrect')
        return render(request, 'login.html')

def pagina_delogare(request):
	logout(request)
	return redirect('login')

@login_required
def pagina_user(request):
    user_detalii = ExtendUser.objects.get(user=request.user)

    if request.method == 'POST':
        form = DetaliiLivrareUser(request.POST, instance=user_detalii)
        if form.is_valid():
            user_detalii.update(form)
            return redirect('index')

    form = DetaliiLivrareUser(initial=model_to_dict(user_detalii))   
    return render(request, 'user.html', {'form': form})

def produs(request, categorie, id):
    p = interogare_produs(categorie, id)
    compara_produse = plural_name_models[categorie].objects.all().exclude(id=id)
    attrs = {}
    if categorie == 'calculatoare':
        for attr in p._meta.fields:
            if attr.name not in ['id', 'nume', 'pret', 'cantitate', 'separat', 'poza']:
                attrs[attr.name.replace('_', ' ').capitalize()] = {}
                obj_pc = getattr(p, attr.name)
                for subattr in obj_pc._meta.fields:
                    if subattr.name not in ['id', 'nume', 'pret', 'cantitate', 'separat', 'poza']:
                        attrs[attr.name.replace('_', ' ').capitalize()][subattr.name.replace('_', ' ').capitalize()] = getattr(obj_pc, subattr.name)
    else:
        for attr in p._meta.fields:
            if attr.name not in ['id', 'nume', 'pret', 'cantitate', 'separat', 'poza']:
                attrs[attr.name.replace('_', ' ').title()] = getattr(p, attr.name)

    return render(request, 'produs.html', {
        'produs': {'query': p, 'attrs': attrs}, 
        'compara_produse': compara_produse,
        'categorie': categorie, 
        'id': id
    })

def compara(request, categorie, id):
    id2 = request.POST.get('id2')
    if not id2:
        raise Http404("Trebuie sa specifici ID-ul produsului cu care vrei sa compari")
    p1 = interogare_produs(categorie=categorie, id=id)
    p2 = interogare_produs(categorie=categorie, id=id2)
    attrs = {}
    for attr in p1._meta.fields:
        if attr.name not in ['id', 'nume', 'pret', 'cantitate', 'separat', 'poza']:
            attrs[attr.name.replace('_', ' ').title()] = (getattr(p1, attr.name), getattr(p2, attr.name))
    return render(request, 'compara.html', {
        'produs1': p1, 
        'produs2': p2, 
        'attrs': attrs, 
        'categorie': categorie, 
        'id': id
    })

@login_required
def review(request, categorie, id):
    if request.method == 'POST':
        r = Review(user=request.user, score=request.POST.get('score'), comment=request.POST.get('comment'))
        r.save()
        p = plural_name_models[categorie].objects.get(id=id)
        p.reviews.add(r)
        return redirect('produs', categorie=categorie, id=id)

def cos(request):
    if 'cart' not in request.session:
        request.session['cart'] = []
    return render(request, 'cos.html', {
        'cos': request.session['cart'],
        'total': sum([x['pret_total'] for x in request.session['cart']])
    })


def checkout(request):
    if request.method == 'POST':
        detalii_livrare = None
        if not request.user.is_authenticated:
            form = DetaliiLivrareNonUser(request.POST)
            if form.is_valid():
                detalii_livrare = form.save()
                comanda = Comanda(extend_user=detalii_livrare, pret_total=0, client=False)
            else:
                return render(request, 'checkout.html', {
                    'user_detalii': None, 
                    'cos': request.session['cart'],
                    'total': sum([x['pret_total'] for x in request.session['cart']]),
                    'form_detalii': form
                })
        else:
            detalii_livrare = ExtendUser.objects.get(user=request.user)
            comanda = Comanda(user=request.user, extend_user=detalii_livrare, pret_total=0, client=True)
        comanda.save()
        pret_total = 0
        for p in request.session['cart']:
            produs = ComandaProdus(nume=p['nume'],
                                   cantitate=p['cantitate'],
                                   pret_unitar=p['pret_unitar'],
                                   pret_total=p['pret_total'],
                                   poza=p['link_poza'])
            pret_total += int(p['pret_total'])
            pquery = interogare_produs(p['categorie'], p['id'])
            pquery.cantitate -= int(p['cantitate'])
            pquery.save()
            produs.save()
            comanda.produse.add(produs)
        comanda.pret_total = pret_total
        comanda.save()
        request.session['cart'] = []
        request.session.modified = True
        return redirect('index')
    else:
        form = DetaliiLivrareNonUser()
        # Daca intri prima oara, pe site, in pagina de checkout, nu vei avea cosul in sesiune
        if 'cart' not in request.session:
            request.session['cart'] = []
        # Redirectioneaza clientii inapoi spre pagina principala
        # Nu pot plasa o comanda daca nu au nimic in cos
        if not request.session['cart']:
            return redirect('index')
        user_detalii = None
        if request.user.is_authenticated:
            user_detalii = ExtendUser.objects.get(user=request.user)
        return render(request, 'checkout.html', {
            'user_detalii': user_detalii, 
            'cos': request.session['cart'],
            'total': sum([x['pret_total'] for x in request.session['cart']]),
            'form_detalii': form
        })

def modifica_cos(request, metoda, categorie, id):
    """
    metoda: adauga/sterge
    """
    if metoda not in ['adauga', 'sterge']:
        raise Http404("Metodele suportate sunt 'adauga' si 'sterge'")
    if 'cart' not in request.session:
        request.session['cart'] = []
    p = interogare_produs(categorie=categorie, id=id)
    found = -1
    for i, cart_iter in enumerate(request.session['cart']):
        if cart_iter['nume'] == p.nume:
            found = i
            break
    if found == -1:
        if metoda == 'sterge':
            return redirect('cos')
        request.session['cart'].append({
            'cantitate': 1, 'pret_unitar': p.pret, 'pret_total': p.pret, 
            'link_poza': p.poza.url, 'nume': p.nume, 'id': id, 'categorie': categorie
        })
    else:
        if metoda == 'sterge':
            request.session['cart'][found]['cantitate'] -= 1
            request.session['cart'][found]['pret_total'] = request.session['cart'][found]['cantitate'] * request.session['cart'][found]['pret_unitar']
            if request.session['cart'][found]['cantitate'] == 0:
                del request.session['cart'][found]
        else:
            if request.session['cart'][found]['cantitate'] + 1 > p.cantitate:
                messages.info(request, 'Cantitatea nu este disponibila')
                return redirect('cos')
            request.session['cart'][found]['cantitate'] += 1
            request.session['cart'][found]['pret_total'] = request.session['cart'][found]['cantitate'] * request.session['cart'][found]['pret_unitar']
    # https://docs.djangoproject.com/en/1.10/topics/http/sessions/#when-sessions-are-saved
    request.session.modified = True
    return redirect('cos')