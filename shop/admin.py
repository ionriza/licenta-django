from django.contrib import admin
from .models import *

class ProdusAdmin(admin.ModelAdmin):
    list_display = ('nume', 'pret', 'cantitate')

admin.site.register(Procesor, ProdusAdmin)
admin.site.register(PlacaDeBaza, ProdusAdmin)
admin.site.register(PlacaVideo, ProdusAdmin)
admin.site.register(Memorie, ProdusAdmin)
admin.site.register(Stocare, ProdusAdmin)
admin.site.register(Sursa, ProdusAdmin)
admin.site.register(Carcasa, ProdusAdmin)
admin.site.register(Software, ProdusAdmin)
admin.site.register(Calculator, ProdusAdmin)
admin.site.register(ExtendUser)
admin.site.register(Review)

class ComandaAdmin(admin.ModelAdmin):
    list_display = ('user', 'extend_user', 'client', 'proceseaza_produse', 'status')

admin.site.register(Comanda, ComandaAdmin)

admin.site.site_header = 'Administrare PC SHOP'